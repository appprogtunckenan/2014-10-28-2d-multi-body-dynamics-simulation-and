#Set of classes to enable dynamic simulations of serial mult-body
#systems in 2d for HW4 of ME397
import numpy as np
import numpy.linalg as lin 
from numpy.linalg import inv

#Encapuslation of a 2d rigid body transform from F0 to F1
#d_ is offset from F0 to F1, represented in F0
#theta_ parametrizes rotation from F0 to F1 
#s.t. x_1 {represented in F0} = R(theta_) x_0
class Transform:
	def __init__(self,d_=np.zeros(2),theta_=0.0):
		# copy initial data to local variables
		self.d = d_
		self.theta = theta_
		# Setup appropriately sized np.arrays for other variabls.
		self.R = np.zeros((2,2))
		# transforms a point represented in F1 
		# to its equivalent representation in F0
		self.T_1to0 = np.zeros((3,3))
		self.T_1to0[2,2] = 1.0
		# transforms a point represented in F0 
		# to its equivalent representation in F1
		self.T_0to1 = np.zeros((3,3))		
		self.T_0to1[2,2] = 1.0
		# Call update to initialize transforms
		self.__update()

	def __update(self):

		#Buranin uzerinden gectik bugun transformation matrixini yaratma
		# Shape tedetest ettim
		#set rotation matrix R(theta)
		#Generate 2 by 2 rotation matrix
		R = np.array([
				 [np.cos(self.theta),-np.sin(self.theta)],
				 [np.sin(self.theta),np.cos(self.theta)]
				 ])
		#TODO
		# create x and y column vector
		col = [[self.d[0]],[self.d[1]]]
		#create the bottom row to make it a square matrix
		row = ([0,0,1])
		#concetenate all to generate transformation matrix
		T = np.append(R, col, 1)
		self.T_1to0 = np.vstack([T, row])
		#set self.T_1to0 
		#TODO

		#Take the inverse of the from 0 to 1
		self.T_0to1 = lin.inv(self.T_1to0)
		#set self.T_0to1
		#TODO
		# raise NotImplementedError()
		
	def updateTheta(self,theta_):	
		self.theta = theta_
		self.__update()

	def getd(self):
		return self.d

	def updateDisplacement(self,d_):
		self.d = d_.squeeze()
		self.__update()

	def getSE2(self):
		return self.T_0to1

	#This is most commonly used to transform joint frame to inertial frame
	def getInverseSE2(self):
		return self.T_1to0