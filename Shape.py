import numpy as np
import matplotlib.pyplot as plt
import matplotlib.path as pth
import matplotlib.patches as patches
import matplotlib.path as path
import matplotlib.animation as anim
from math import sin, cos

from Transform import *
#Base class for shapes (which will )
#All shapes are defined w.r.t. a link (parent jonit) frame, 
#with an initial (static) rigid body transform (T0) relative to that frame 
#IMPROVE: make this an abstract base class
class Shape:
    def __init__(self,name_,transform_=Transform(np.zeros(2),0.0)):
        self.name = name_
        self.T0 = transform_

    def getStaticTransform(self):
        return self.T0

    #update internal variables from a transform object
    #baseTransform maps the parent frame to the inertial frame
    def update(self, baseTransform_=Transform(np.zeros(2),0.0)):
        #updates internal variables in preparation for drawing a figure
        raise NotImplementedError()

    #update internal variables from a transform represented as a matrix
    #baseTransform maps the parent frame to the inertial frame
    def updateFromMatrix(self, baseTMatrix_):
        #updates internal variables in preparation for drawing a figure
        raise NotImplementedError()

    #draw adds the object (e.g. a patch) to the axis ax_
    def draw(self, ax_):
        #draws the object
        # ax_.add_patch(patch)
        raise NotImplementedError()

#Line from p1 to p2
class Line(Shape):
    def __init__(self,name_,p1_,p2_,transform_=Transform(np.zeros(2),0.0)):
        Shape.__init__(self,name_,transform_)
        #initialize member data
        self.p1 = p1_
        self.p2 = p2_
        #initial vertices
        self.v0 = [p1_,p2_]
        #fully transformed vertices
        self.vertices = [p1_,p2_]
        self.update()

    def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
        baseT = baseTransform_.getInverseSE2()
        self.updateFromMatrix(baseT)

    def updateFromMatrix(self, baseTMatrix_):
        T0 = np.dot(baseTMatrix_,self.T0.getInverseSE2())
        self.vertices[0] = np.dot(T0[0:2,0:2],self.p1) + T0[0:2,2]
        self.vertices[1] = np.dot(T0[0:2,0:2],self.p2) + T0[0:2,2]

    def draw(self, ax_, color_='r',lw_=2):
        #Note: use of patch not completely necessary...but useful hint for rectangle implementation
        vtemp = self.vertices[:]
        codes = [pth.Path.MOVETO,pth.Path.LINETO]
        path = pth.Path(vtemp,codes)
        patch = patches.PathPatch(path,facecolor=color_,lw=lw_)
        ax_.add_patch(patch)

#Rectangle centered at (0,0) with width in x-direction and height in y-direction
class Rectangle(Shape):
    def __init__(self,name_,width_,height_,transform_=Transform(np.zeros(2),0.0)):
        Shape.__init__(self,name_,transform_)
        self.width = width_
        self.height = height_
        h2=height_*0.5
        w2=width_*0.5
        self.v0 = np.array([[w2,-w2,-w2,w2,w2],[h2,h2,-h2,-h2,h2]])
        self.vertices = np.zeros((5,2))

        self.codes = np.ones(5, int) * path.Path.LINETO
        self.codes[0] = path.Path.MOVETO
        self.codes[4] = path.Path.CLOSEPOLY
        self.rectangle_path=path.Path(self.vertices, self.codes)
        self.updateFromMatrix(np.identity(3))

    def setMatrixFetchFunction(self, matrix_fetcher):
        # add a matrix_fetcher member, which is a funciton
        self.fetch_matrix= matrix_fetcher

    def animate(self):
        # call matrix_fetcher as a function
        self.updateFromMatrix(self.fetch_matrix())

    def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
        baseT = baseTransform_.getInverseSE2()
        self.updateFromMatrix(baseT)

    def updateFromMatrix(self, baseTMatrix_):
        for i in range(0,5):
            self.vertices[i,:]=(baseTMatrix_[0:2,0:2].dot(self.v0[:,i])
                    +baseTMatrix_[0:2,2]).T

    def draw(self, ax_, color='green', **kwargs):
        self.patch = patches.PathPatch(self.rectangle_path,
                 facecolor=color, edgecolor='yellow', alpha=0.5, **kwargs)
        ax_.add_patch(self.patch)

#Burada circle uygulandi kontrol edildi
class Circle(Shape):
    def __init__(self,name_,radius_,transform_=Transform(np.zeros(2),0.0)):
        Shape.__init__(self,name_,transform_)
        self.radius = radius_
        #Set the initial center locations using transform
        self.center = transform_.getInverseSE2()[0:2,2]

        #Create center using 26 vertices
        # Source: http://www.ast.cam.ac.uk/~tcollett/pylensdistro/pylensdistro/justincase/python/matplotlib-1.1.0/matplotlib/path.py
        MAGIC = 0.2652031
        SQRTHALF = np.sqrt(0.5)
        MAGIC45 = np.sqrt((MAGIC*MAGIC) / 2.0)
        V_circle = np.array([[0.0, -1.0],
                             [MAGIC, -1.0],
                             [SQRTHALF-MAGIC45, -SQRTHALF-MAGIC45],
                             [SQRTHALF, -SQRTHALF],
                             [SQRTHALF+MAGIC45, -SQRTHALF+MAGIC45],
                             [1.0, -MAGIC],
                             [1.0, 0.0],
                             [1.0, MAGIC],
                             [SQRTHALF+MAGIC45, SQRTHALF-MAGIC45],
                             [SQRTHALF, SQRTHALF],
                             [SQRTHALF-MAGIC45, SQRTHALF+MAGIC45],
                             [MAGIC, 1.0],
                             [0.0, 1.0],
                             [-MAGIC, 1.0],
                             [-SQRTHALF+MAGIC45, SQRTHALF+MAGIC45],
                             [-SQRTHALF, SQRTHALF],
                             [-SQRTHALF-MAGIC45, SQRTHALF-MAGIC45],
                             [-1.0, MAGIC],
                             [-1.0, 0.0],
                             [-1.0, -MAGIC],
                             [-SQRTHALF-MAGIC45, -SQRTHALF+MAGIC45],
                             [-SQRTHALF, -SQRTHALF],
                             [-SQRTHALF+MAGIC45, -SQRTHALF-MAGIC45],
                             [-MAGIC, -1.0],
                             [0.0, -1.0],
                             [0.0, -1.0]],
                            dtype=np.float_)
		#Create initial vertices
        self.v0 = V_circle * radius_ + self.center
        #Create vertices and path to update
        self.vertices = np.zeros((26,2))
        self.codes = [path.Path.CURVE4] * 26
        self.codes[0] = path.Path.MOVETO
        self.codes[25] = path.Path.CLOSEPOLY
        self.circle_path = path.Path(self.vertices, self.codes)
        self.updateFromMatrix(np.identity(3))

    def setMatrixFetchFunction(self, matrix_fetcher):
        # add a matrix_fetcher member, which is a funciton
        self.fetch_matrix= matrix_fetcher

    def animate(self):
        # call matrix_fetcher as a function
        self.updateFromMatrix(self.fetch_matrix())

    def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
        baseT = baseTransform_.getInverseSE2()
        self.updateFromMatrix(baseT)

    def updateFromMatrix(self, baseTMatrix_):

    	#Update vertices using transformation matrix
        for i in range(0,26):
            self.vertices[i,:]=(baseTMatrix_[0:2,0:2].dot(self.v0[i,:])+baseTMatrix_[0:2,2]).T

        # raise NotImplementedError()
        # Calculate net transform T0 and apply it to the center of the circle
        # TODO     

    def draw(self,ax_,color='r', **kwargs):

    	#Draw the circle implemented similar to rectangle
            self.patch = patches.PathPatch(self.circle_path,
                 facecolor=color, edgecolor='yellow', alpha=0.5, **kwargs)
            ax_.add_patch(self.patch)
        # raise NotImplementedError()
        # TODO: create a circular patch and add it ax_
        
#Implement transform with circle using Matrix fetch function, this is seperated from the rest of the simulation
#Can be seen by running the Shape.py alone       
if __name__=="__main__":
    print "hello world"
    fig, ax=plt.subplots()
    ax.axis('equal')
    ax.axis([-5,5,-5,5])

    rect=Rectangle("rect 1", 0.4, 0.4)
    rect2=Rectangle("rect 2", 0.8, 0.1)
    circle = Circle("circ 1",1)
    circle.draw(ax)
    rect.draw(ax)
    rect2.draw(ax)

    transform_object=Transform()
    trans=np.array([
            [1.0,0.0,0.0],
            [0.0,1.0,0.0],
            [0.0,0.0,1.0]
            ])
    rect.setMatrixFetchFunction(transform_object.getInverseSE2)
    rect2.setMatrixFetchFunction(transform_object.getInverseSE2)
    circle.setMatrixFetchFunction(transform_object.getInverseSE2)
    animateables=[rect, rect2,circle]
    lambda x : x+1
    def add_1(x):
        return x+1
    add_1(2) # should be 3

    def pose_objects(i):


        theta=i*0.1+2
        x=0.0+5*sin(i*0.01)
        y=2
        d = np.array([x,y])
        transform_object.updateTheta(theta)
        transform_object.updateDisplacement(d)
        
    import time
    import thread
    pose_number=0
    def loop_poses():
        global pose_number
        if pose_number >= 50:
            pose_number=0
        pose_objects(pose_number)
        pose_number+=1
    def loop_poses_forever():
        while True:
            loop_poses()
            time.sleep(0.02)

    thread.start_new_thread(loop_poses_forever,())

    def animate(i):

        for animateable in animateables:
            animateable.animate()
    ani=anim.FuncAnimation(fig, animate, range(0,50),interval=20)
    plt.show()
