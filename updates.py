
###############################################################################################################################
# From Transform
def __update(self):

	#Buranin uzerinden gectik bugun transformation matrixini yaratma
	# Shape tedetest ettim
	#set rotation matrix R(theta)
	#Generate 2 by 2 rotation matrix
	R = np.array([
			 [np.cos(self.theta),-np.sin(self.theta)],
			 [np.sin(self.theta),np.cos(self.theta)]
			 ])
	#TODO
	# create x and y column vector
	col = [[self.d[0]],[self.d[1]]]
	#create the bottom row to make it a square matrix
	row = ([0,0,1])
	#concetenate all to generate transformation matrix
	T = np.append(R, col, 1)
	self.T_1to0 = np.vstack([T, row])
	#set self.T_1to0 
	#TODO

	#Take the inverse of the from 0 to 1
	self.T_0to1 = lin.inv(self.T_1to0)
	#set self.T_0to1
	#TODO
	# raise NotImplementedError()
###############################################################################################################################
# From Shape
class Circle(Shape):
    def __init__(self,name_,radius_,transform_=Transform(np.zeros(2),0.0)):
        Shape.__init__(self,name_,transform_)
        self.radius = radius_
        #Set the initial center locations using transform
        self.center = transform_.getInverseSE2()[0:2,2]

        #Create center using 26 vertices
        # Source: http://www.ast.cam.ac.uk/~tcollett/pylensdistro/pylensdistro/justincase/python/matplotlib-1.1.0/matplotlib/path.py
        MAGIC = 0.2652031
        SQRTHALF = np.sqrt(0.5)
        MAGIC45 = np.sqrt((MAGIC*MAGIC) / 2.0)
        V_circle = np.array([[0.0, -1.0],
                             [MAGIC, -1.0],
                             [SQRTHALF-MAGIC45, -SQRTHALF-MAGIC45],
                             [SQRTHALF, -SQRTHALF],
                             [SQRTHALF+MAGIC45, -SQRTHALF+MAGIC45],
                             [1.0, -MAGIC],
                             [1.0, 0.0],
                             [1.0, MAGIC],
                             [SQRTHALF+MAGIC45, SQRTHALF-MAGIC45],
                             [SQRTHALF, SQRTHALF],
                             [SQRTHALF-MAGIC45, SQRTHALF+MAGIC45],
                             [MAGIC, 1.0],
                             [0.0, 1.0],
                             [-MAGIC, 1.0],
                             [-SQRTHALF+MAGIC45, SQRTHALF+MAGIC45],
                             [-SQRTHALF, SQRTHALF],
                             [-SQRTHALF-MAGIC45, SQRTHALF-MAGIC45],
                             [-1.0, MAGIC],
                             [-1.0, 0.0],
                             [-1.0, -MAGIC],
                             [-SQRTHALF-MAGIC45, -SQRTHALF+MAGIC45],
                             [-SQRTHALF, -SQRTHALF],
                             [-SQRTHALF+MAGIC45, -SQRTHALF-MAGIC45],
                             [-MAGIC, -1.0],
                             [0.0, -1.0],
                             [0.0, -1.0]],
                            dtype=np.float_)
		#Create initial vertices
        self.v0 = V_circle * radius_ + self.center
        #Create vertices and path to update
        self.vertices = np.zeros((26,2))
        self.codes = [path.Path.CURVE4] * 26
        self.codes[0] = path.Path.MOVETO
        self.codes[25] = path.Path.CLOSEPOLY
        self.circle_path = path.Path(self.vertices, self.codes)
        self.updateFromMatrix(np.identity(3))

    def updateFromMatrix(self, baseTMatrix_):

    	#Update vertices using transformation matrix
        for i in range(0,26):
            self.vertices[i,:]=(baseTMatrix_[0:2,0:2].dot(self.v0[i,:])+baseTMatrix_[0:2,2]).T

        # raise NotImplementedError()
        # Calculate net transform T0 and apply it to the center of the circle
        # TODO     

    def draw(self,ax_,color='r', **kwargs):

    	#Draw the circle implemented similar to rectangle
            self.patch = patches.PathPatch(self.circle_path,
                 facecolor=color, edgecolor='yellow', alpha=0.5, **kwargs)
            ax_.add_patch(self.patch)
        # raise NotImplementedError()
        # TODO: create a circular patch and add it ax_

#Implement transform with circle using Matrix fetch function, this is seperated from the rest of the simulation
#Can be seen by running the Shape.py alone       
if __name__=="__main__":
    print "hello world"
    fig, ax=plt.subplots()
    ax.axis('equal')
    ax.axis([-5,5,-5,5])

    rect=Rectangle("rect 1", 0.4, 0.4)
    rect2=Rectangle("rect 2", 0.8, 0.1)
    circle = Circle("circ 1",1)
    circle.draw(ax)
    rect.draw(ax)
    rect2.draw(ax)

    transform_object=Transform()
    trans=np.array([
            [1.0,0.0,0.0],
            [0.0,1.0,0.0],
            [0.0,0.0,1.0]
            ])
    rect.setMatrixFetchFunction(transform_object.getInverseSE2)
    rect2.setMatrixFetchFunction(transform_object.getInverseSE2)
    circle.setMatrixFetchFunction(transform_object.getInverseSE2)
    animateables=[rect, rect2,circle]
    lambda x : x+1
    def add_1(x):
        return x+1
    add_1(2) # should be 3

    def pose_objects(i):


        theta=i*0.1+2
        x=0.0+5*sin(i*0.01)
        y=2
        d = np.array([x,y])
        transform_object.updateTheta(theta)
        transform_object.updateDisplacement(d)
        
    import time
    import thread
    pose_number=0
    def loop_poses():
        global pose_number
        if pose_number >= 50:
            pose_number=0
        pose_objects(pose_number)
        pose_number+=1
    def loop_poses_forever():
        while True:
            loop_poses()
            time.sleep(0.02)

    thread.start_new_thread(loop_poses_forever,())

    def animate(i):

        for animateable in animateables:
            animateable.animate()
    ani=anim.FuncAnimation(fig, animate, range(0,50),interval=20)
    plt.show()

###############################################################################################################################
#From RigidBody

def __setInertia(self,inertia_):
	#TODO: parallel axis theorem so rotational inertia is defined w.r.t.the joint frame
	#I_joint <-- I_com + d^2*M
	#Updated according to the given formula
	M = inertia_.getM()
	I = inertia_.getI()
	COM = inertia_.getCOM()
	d = (COM[0]**2+COM[1]**2)**0.5

	I_com = I+ (d**2)*M
	#Set the new inertia w.r.t. joint frame 
	inertia_.setParameters(M_= M,I_ = I_com, COM_=COM)
	# raise NotImplementedError()

def update(self,q):

	#Depending on q there are 3 possible joints theta (revolute) , x and y (prismatic)
	if self.axisIdx == 0: #revolute joint
		#TODO: update self.TJ for a revolute
		#if revolute update theta
		self.TJ.updateTheta(q)
		# raise NotImplementedError()
	else: #prismatic joint 
		#TODO: update self.TJ
		#if prismatic joint update the displacement with x or y by multiplying the given q with direction unit vectors
		# axisID=1 => x axisID=2 => y
		q_x =q*np.array([1,0])
		q_y =q*np.array([0,1])
		if self.axisIdx == 1:
			self.TJ.updateDisplacement(q_x)
		else:
			self.TJ.updateDisplacement(q_y)
		# raise NotImplementedError()

def getIdx(self):
	#TODO: return this joint's index based on parent index (and npn-branching assumption)
	return self.parentIdx+1

def getTransformAsMatrix(self):
	# return full joint transform given by T0 * TJ
	return np.dot(self.T0.getInverseSE2(),self.TJ.getInverseSE2())
	# raise NotImplementedError()

###############################################################################################################################
#From SerialRobot

def __setup(self):
	jtemp = self.joints[:]
	#TODO: (using jtemp) sort the list of joints so order is 0-->N
	#if multiple joints have the same index, this overwrites
	#bubble sorting algorithm for joint index
	not_complete = True
	j_list = []
	while not_complete:
		not_complete = False
		for jj in range(0,len(jtemp)):
			if jj == len(jtemp)-1: 
				jj = 0
			else:
				if jtemp[jj].getIdx() > jtemp[jj+1].getIdx():
					jtemp[jj], jtemp[jj+1] = jtemp[jj+1], jtemp[jj]
					not_complete = True

		if not_complete == True:
			for jj in range(0,len(jtemp)):
				j_list.append(jtemp[jj].getIdx())


	# print j_list				
	#TODO: itialize self.transforms to
	#list of recursive transforms for each frame mapping 
	#(point in joint frame)-->(point in inertial reference)
	#self.transforms[j]  = T[j] = T[0]*T[1] * ... * T[j]

	#Set the joint transformation matrices
	for jj in range(0,len(jtemp)):

		# for the first joint append the initial transformation
		if jj == 0:
			self.transforms.append(jtemp[jj].getTransformAsMatrix())
		#For the rest append the multiplication with previous transformation matrix 
		else:
			self.transforms.append(np.dot(self.transforms[jj-1],jtemp[jj].getTransformAsMatrix())) 

	#Set the joints
	self.joints = jtemp

# calculates and returns configuration dependent joint space mass matrix
def getMassMatrix(self):
	M = np.zeros((self.ndofs,self.ndofs))
	#TODO: calcualte mass joint space matrix
	#Update mass matrix
	for jj in range(0,self.ndofs):

		#get CoM location in given link
		point = self.joints[jj].link.inertia.getCOM()
		#Jacobian w.r.t. joint 
		J_r = self.getJacobian(jj)
		#Jacobian w.r.t. CoM
		J_s = self.getJacobian(jj,point)
		#get q (angle or displacement)
		q_s = self.q[jj]
		#get the mass of the link
		mass = self.joints[jj].link.inertia.getM()
		#Generate the mass matrix according to formula
		M +=  np.dot((np.transpose(J_s)*mass),J_s) + np.dot((np.transpose(J_r)*q_s),J_r)

	return M

# calculates and returns joint-space gravity vector
def getGravity(self):
	g = np.zeros(self.ndofs)
	#TODO : calculate joint space gravity vector
	for jj in range(0,self.ndofs):

		#get CoM location in given link
		point = self.joints[jj].link.inertia.getCOM()
		#Jacobian w.r.t. CoM
		J = self.getJacobian(jj,point)
		#get the mass of the link
		mass = self.joints[jj].link.inertia.getM()
		#Set the gravitational force
		F = np.array([0,mass*self.grav[0],mass*self.grav[1]])
		#Update gravity vector
		g += -np.dot(np.transpose(J),F)
	return g

#returns the point_ represented in inertial reference frame
def getPoint(self,jointIdx_,point_=np.zeros(2)):
	#TODO: use self.transforms to calculate p_inertial = Tj * point_

	#Get the transformation matrix of the joint
	TJ = self.transforms[jointIdx_]
	#Transform the point
	p_i = np.dot(TJ,np.concatenate((point_, [1]), axis=0))
	p_inertia = p_i[0:2]

	return p_inertia

# updates each robot joint and recursive transform to that joint
def update(self):		
	for jj in range(0,len(self.joints)):
		# raise NotImplementedError()
		#Update individual joints
		#TODO: update joint jj using self.q[jj]

		#Update the type of the joint
		self.joints[jj].update(self.q[jj])
		#TODO: update recursive transform self.transform[jj]
		# Update joint transformation matrices similar to setup except set each of them individually instead of append
		if jj == 0:
			self.transforms[jj] = self.joints[jj].getTransformAsMatrix()
		else:
			self.transforms[jj] = np.dot(self.transforms[jj-1],self.joints[jj].getTransformAsMatrix())

###############################################################################################################################
#From RobotForces

#Updates forces/Jacobians for robot_ (already updated) to current position
def update(self,robot_=SerialRobot("default")):
	#Get collisions and current transform
	#Get the collisions
	coll = robot_.getLink(self.idx).collisions
	#Get the transformation matrix from world to joint
	T = robot_.getTransformAsMatrix(self.idx)
	#For each collision....
	for ii in range(0,len(coll)):
		#TODO: update the collision
		#TODO: calculate the (signed) distance from the circle center to the plane
		#TODO: calculate the (signed) distance from the edge of the circle to the plane

		#Get the location of the contact circle center in world coordinate frame
		center = (np.dot(T[0:2,0:2],coll[ii].center) +T[0:2,2])
		#Get the radius of the contact circle
		radius = coll[ii].radius

		#Get the vector from contact circle center to ground
		v_g_c = [center[0]-self.point[0],center[1]-self.point[1]] 
		#Get the vertical distance from center
		dist_c = (np.dot(v_g_c,self.normal))/(np.linalg.norm(self.normal))
		#Get the vertical distance from the edge point closest to ground
		dist_e = dist_c-radius

		#If the ground is penetrated
		if(dist_c <= 0): #contact has occured! 
			#TODO calculate the velocity of the point

			#Get the static transform of circle in joint coordinate frame
			T_object = coll[ii].getStaticTransform()
			T0 = T_object.getInverseSE2()
			#Get the location of center point
			point_j = T0[0:2,2]

			#Get jacobian w.r.t. contact point
			J_coll = robot_.getJacobian(self.idx,point_j)
			#trim the jacobian since it is only translational
			J_final = J_coll[1:,:]
			#Get the velocity of the point
			v_point = np.dot(J_final,np.transpose(robot_.qdot))
			#Penetration from edge point
			p_d = abs(dist_e-self.point)
			#Generate ground reaction force
			self.F[ii] = -(self.K*self.normal*p_d - self.D*v_point)
			#TODO set penalty-based force F = K*self.normal*|d| - D*v_point
			#Update jacobian
			self.J[ii] = J_final
			#TODO update Jacobian
		else: #no contact
			#IMPROVE: currently, the size of these doesn't change at run-time
			#...so we multiply lots of extra zeros.  Would be nice to fix this

			self.F[ii] = np.zeros(2)
			self.J[ii] = np.zeros((2,robot_.ndofs))
		
def update(self,robot_):
	#TODO: Calculate vector from anchor point1 to anchor point2
	v_p = robot_.getPoint(self.idx2,self.p2) - robot_.getPoint(self.idx1,self.p1)
	#TODO: Calculate magnitude of spring force given initial pre-load
	L = sqrt(v_p[0]**2 + v_p[1]**2)
	#change of length on spring
	L_change  = L - self.L0

	#TODO: Calculate and update spring forces and Jacobians
	#get the magnitude of the muscle force
	self.mag = self.K * L_change
	#get the direction of the muscle force
	self.dir = v_p/(np.linalg.norm(v_p))
	#get the jacobians w.r.t. muscles ancor points
	J_1 = robot_.getJacobian(self.idx1,self.p1)
	J_2 = robot_.getJacobian(self.idx2,self.p2)
	#Calculate the forces for each end
	self.F[0] = self.mag*self.dir
	self.F[1] = -(self.mag*self.dir)
	#Update the jacobians (translation only)
	self.J[0] = J_1[1:,:]
	self.J[1] = J_2[1:,:]

###############################################################################################################################
#From RobotSimulation


def __updateInertial(self):

	#Update mass matrix and gravity vector
	self.M = self.robot.getMassMatrix()
	self.g = self.robot.getGravity()
	# raise NotImplementedError()
	#TODO: update self.M and self.g using self.robot method calls

def __getQddot(self, q_, qdot_):
	self.robot.setState(q_,qdot_)
	self.robot.update()
	self.__updateForces()
	self.__updateInertial()
	# Get the RHS of the equation (-g-J*F)
	rhs = -(self.g + np.dot(np.transpose(self.concatForceJac),self.concatForce))
	#get the accelerations
	qddot = np.dot(np.linalg.inv(self.M),rhs)
	return qddot
	#TODO: solve qddot = M^-1 * rhs (see assignment) and return qddot