from SerialRobot import *
from math import sqrt, fabs

#base class to encapsulate configuration-dependent forces on the robot
#IMPROVE: make this an abstract base class
class RobotForce:
	def __init__(self, name_):
		self.name = name_
		self.F = []
		self.J = []

	def getForce(self):
		return self.F

	def getJacobian(self):
		return self.J

	def update(self,robot_=SerialRobot("default")):
		print "NOT IMPLEMENTED"
		pass

#external contact forces via penalty method with damping
#for now this is explicit for circles and half-planes
#BONUS: extend to line/rectangle vertices
class ExternalContact(RobotForce):
	def __init__(self, name_, planePoint_, planeNormal_, K_, D_, jointIdx_,
				 robot_=SerialRobot("default")):
		RobotForce.__init__(self,name_)
		self.K = K_
		self.D = D_
		self.point = planePoint_
		self.normal = planeNormal_
		self.idx = jointIdx_
		#IMPROVE: better error checking at initialization here, assertion on type of shape
		if robot_.ndofs>0:
			self.__setup(robot_)
			self.update(robot_)
		else:
			"WARNING: CONTACT FORCE ON BLANK ROBOT"

	#setup containers for forces and associated Jacobians
	#IMPROVE: check that contact shapes are all circles OR extend to vertices
	def __setup(self,robot_=SerialRobot("default")):
		#Get the list of collision objects associated with this frame
		collisions = robot_.getLink(self.idx).collisions
		# Expect a 2d force for each circle/plane collision
		# (more for vertex-based collisions)
		for ii in range(0,len(collisions)):
			self.F.append(np.zeros(2))
			self.J.append(np.zeros((2,robot_.ndofs)))

	#Updates forces/Jacobians for robot_ (already updated) to current position
	def update(self,robot_=SerialRobot("default")):
		#Get collisions and current transform
		#Get the collisions
		coll = robot_.getLink(self.idx).collisions
		#Get the transformation matrix from world to joint
		T = robot_.getTransformAsMatrix(self.idx)
		#For each collision....
		for ii in range(0,len(coll)):
			#TODO: update the collision
			#TODO: calculate the (signed) distance from the circle center to the plane
			#TODO: calculate the (signed) distance from the edge of the circle to the plane

			#Get the location of the contact circle center in world coordinate frame
			center = (np.dot(T[0:2,0:2],coll[ii].center) +T[0:2,2])
			#Get the radius of the contact circle
			radius = coll[ii].radius

			#Get the vector from contact circle center to ground
			v_g_c = [center[0]-self.point[0],center[1]-self.point[1]] 
			#Get the vertical distance from center
			dist_c = (np.dot(v_g_c,self.normal))/(np.linalg.norm(self.normal))
			#Get the vertical distance from the edge point closest to ground
			dist_e = dist_c-radius

			#If the ground is penetrated
			if(dist_c <= 0): #contact has occured! 
				#TODO calculate the velocity of the point

				#Get the static transform of circle in joint coordinate frame
				T_object = coll[ii].getStaticTransform()
				T0 = T_object.getInverseSE2()
				#Get the location of center point
				point_j = T0[0:2,2]

				#Get jacobian w.r.t. contact point
				J_coll = robot_.getJacobian(self.idx,point_j)
				#trim the jacobian since it is only translational
				J_final = J_coll[1:,:]
				#Get the velocity of the point
				v_point = np.dot(J_final,np.transpose(robot_.qdot))
				#Penetration from edge point
				p_d = abs(dist_e-self.point)
				#Generate ground reaction force
				self.F[ii] = -(self.K*self.normal*p_d - self.D*v_point)
				#TODO set penalty-based force F = K*self.normal*|d| - D*v_point
				#Update jacobian
				self.J[ii] = J_final
				#TODO update Jacobian
			else: #no contact
				#IMPROVE: currently, the size of these doesn't change at run-time
				#...so we multiply lots of extra zeros.  Would be nice to fix this

				self.F[ii] = np.zeros(2)
				self.J[ii] = np.zeros((2,robot_.ndofs))
			
#elastic muscles 
class LinearMuscle(RobotForce):
	def __init__(self,name_,K_,joint1Idx_,point1_,joint2Idx_,point2_,preloadDelta_,
				 robot_=SerialRobot("default")):
		RobotForce.__init__(self,name_)
		self.K = K_
		self.idx1 = joint1Idx_
		self.p1 = point1_
		self.idx2 = joint2Idx_
		self.p2 = point2_
		self.L0 = 0
		self.mag = 0
		self.dir = np.zeros(2)
		if robot_.ndofs>0:
			self.__setup(preloadDelta_,robot_)
			self.update(robot_)
		else:
			"WARNING: MUSCLE FORCE ON BLANK ROBOT"

	def __setup(self,preloadDelta_,robot_=SerialRobot("default")):
		#Treating muscles as external forces gives equal and opposite forces on 2 joints
		for ii in range(0,2):
			self.F.append(np.zeros(2))
			self.J.append(np.zeros((2,robot_.ndofs)))
		p1_inertial = robot_.getPoint(self.idx1,self.p1)
		p2_inertial = robot_.getPoint(self.idx2,self.p2)
		d0 = p2_inertial - p1_inertial
		L0 = sqrt(d0[0]**2 + d0[1]**2)
		self.L0 = fabs(L0-preloadDelta_)

	def update(self,robot_):
		#TODO: Calculate vector from anchor point1 to anchor point2
		v_p = robot_.getPoint(self.idx2,self.p2) - robot_.getPoint(self.idx1,self.p1)
		#TODO: Calculate magnitude of spring force given initial pre-load
		L = sqrt(v_p[0]**2 + v_p[1]**2)
		#change of length on spring
		L_change  = L - self.L0

		#TODO: Calculate and update spring forces and Jacobians
		#get the magnitude of the muscle force
		self.mag = self.K * L_change
		#get the direction of the muscle force
		self.dir = v_p/(np.linalg.norm(v_p))
		#get the jacobians w.r.t. muscles ancor points
		J_1 = robot_.getJacobian(self.idx1,self.p1)
		J_2 = robot_.getJacobian(self.idx2,self.p2)
		#Calculate the forces for each end
		self.F[0] = self.mag*self.dir
		self.F[1] = -(self.mag*self.dir)
		#Update the jacobians (translation only)
		self.J[0] = J_1[1:,:]
		self.J[1] = J_2[1:,:]
